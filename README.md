# ArgoCD deployment

## creates a Kubernetes namespace called "argocd"
~~~
kubectl create namespace argocd
~~~

## Install Argo CD
~~~
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
~~~

## Get a password to your ArgoCD server
~~~
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d
~~~

## Port Forwarding
~~~
kubectl port-forward svc/argocd-server -n argocd 8080:443
~~~

## Connect to the server

The API server can then be accessed using: https://localhost:8080
<br>username: admin</br>
<br>password: the output from the previous step</br>

### Delete ArgoCD from the cicd namespace
~~~
kubectl delete -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl delete namespace argocd
~~~